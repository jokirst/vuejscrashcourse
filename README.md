# Vue.js ToDo App

A simple ToDo App in Vue.js as shown by Tutorial Creator Brad Traversy on Youtube.

## To start the App on localhost:8080

```
npm run serve
```


## Tutorial

[Tutorial on Youtube](https://www.youtube.com/watch?v=Wy9q22isx3U)

## To support the tutorial creator Brad Traversy

[Brad Traversy's Patreon-Page](http://www.patreon.com/traversymedia)
